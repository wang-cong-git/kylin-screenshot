<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Cut</name>
    <message>
        <source>cut</source>
        <translation>དྲས་གཏུབ་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>Font_Options</name>
    <message>
        <source>Bold</source>
        <translation>སྦོས་གཟུགས།</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>གསེག་གཟུགས།</translation>
    </message>
    <message>
        <source>StrikeOut</source>
        <translation>བསུབ་ཐིགས།</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation>འོག་ཐིག</translation>
    </message>
</context>
<context>
    <name>Font_Options2</name>
    <message>
        <source>Bold</source>
        <translation>སྦོས་གཟུགས།</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>གསེག་གཟུགས།</translation>
    </message>
    <message>
        <source>StrikeOut</source>
        <translation>བསུབ་ཐིགས།</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation>འོག་ཐིག</translation>
    </message>
</context>
<context>
    <name>TextConfig</name>
    <message>
        <source>Bold</source>
        <translation>སྦོས་གཟུགས།</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>གསེག་གཟུགས།</translation>
    </message>
    <message>
        <source>StrikeOut</source>
        <translation>བསུབ་ཐིགས།</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation>འོག་ཐིག</translation>
    </message>
</context>
<context>
    <name>CopyTool</name>
    <message>
        <source>Copy</source>
        <translation>འདྲ་ཕབ།</translation>
    </message>
    <message>
        <source>Copy the selection into the clipboard</source>
        <translation>འདྲ་བཟོ་བྱས་ནས་དྲས་སྦྱར་པང་ལེབ་ལ་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>ExitTool</name>
    <message>
        <source>Exit</source>
        <translation> ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>Leave the capture screen</source>
        <translation>འཆར་ངོས་དང་བྲལ་ནས་ལེན་པ།</translation>
    </message>
</context>
<context>
    <name>LineTool</name>
    <message>
        <source>Line</source>
        <translation>སྐུད་ལམ།</translation>
    </message>
    <message>
        <source>Set the Line as the paint tool</source>
        <translation>དྲང་ཐིག་དེ་རི་མོའི་ཡོ་བྱད་དུ་སྒྲིག་བཀོད་བྱེད།</translation>
    </message>
</context>
<context>
    <name>MoveTool</name>
    <message>
        <source>Move</source>
        <translation>སྤོ་བ།</translation>
    </message>
    <message>
        <source>Move the selection area</source>
        <translation>གདམ་ཁོངས་སྒུལ་བ།</translation>
    </message>
</context>
<context>
    <name>RedoTool</name>
    <message>
        <source>Redo</source>
        <translation>བསྐྱར་བཟོ།</translation>
    </message>
    <message>
        <source>Redo the next modification</source>
        <translation>ཐེངས་སྔོན་མར་དག་བཅོས།</translation>
    </message>
</context>
<context>
    <name>CaptureButton</name>
    <message>
        <source>Save</source>
        <translation>བསྒྱུར་བཅོས་ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>option</source>
        <translation>འདེམས་ཚན།</translation>
    </message>
</context>
<context>
    <name>FileNameEditor</name>
    <message>
        <source>Save</source>
        <translation>བསྒྱུར་བཅོས་ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>གཙང་སེལ།</translation>
    </message>
    <message>
        <source>Edit:</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>བསྐྱར་འཇོག</translation>
    </message>
    <message>
        <source>Deletes the name</source>
        <translation>མིང་འདི་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Saves the pattern</source>
        <translation>རྣམ་པ་སོར་འཇོག</translation>
    </message>
    <message>
        <source>Edit the name of your captures:</source>
        <translation>ཁྱེད་ཀྱི་དྲས་གཏུབ་པར་རིས་ལ་མིང་ཐོགས།</translation>
    </message>
    <message>
        <source>Restores the saved pattern</source>
        <translation>ཉར་ཚགས་ཀྱི་རྣམ་པ་སླར་གསོ།</translation>
    </message>
    <message>
        <source>Preview:</source>
        <translation>སྔོན་ལྟ།</translation>
    </message>
</context>
<context>
    <name>PinTool</name>
    <message>
        <source>Pin Tool</source>
        <translation>གཏན་འཇགས་ལག་ཆའི་སྡེ།</translation>
    </message>
    <message>
        <source>Pin image on the desktop  exit with Esc</source>
        <translation>ངོས་གཙོ་བོ་ནས་པར་རིས་གཏན་འཁེལ། Escསྣོན་ནས་ཕྱི་ནུར་བྱོས།</translation>
    </message>
</context>
<context>
    <name>BlurTool</name>
    <message>
        <source>blur</source>
        <translation>རབ་རིབ།</translation>
    </message>
    <message>
        <source>Set Blur as the paint tool</source>
        <translation>རབ་རིབ་པར་རིས་ཡོ་བྱད་དུ་འདེམ།</translation>
    </message>
</context>
<context>
    <name>SaveTool</name>
    <message>
        <source>save</source>
        <translation>保存设置</translation>
    </message>
    <message>
        <source>Save the capture</source>
        <translation>ཐོབ་པ་ཉར་ཚགས།</translation>
    </message>
</context>
<context>
    <name>ssrtools</name>
    <message>
        <source>save</source>
        <translation>保存设置</translation>
    </message>
    <message>
        <source>option</source>
        <translation>འདེམས་ཚན།</translation>
    </message>
</context>
<context>
    <name>TextTool</name>
    <message>
        <source>text</source>
        <translation>ཡིག་རྐྱང།</translation>
    </message>
    <message>
        <source>Add text to your capture</source>
        <translation>ཁྱེད་ཀྱི་ཐོབ་ཁོངས་དུ་རྩོམ་ཆོས་ཁ་སྣོན་བྱོས།</translation>
    </message>
</context>
<context>
    <name>UndoTool</name>
    <message>
        <source>undo</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Undo the last modification</source>
        <translation>ཐེངས་སྔོན་མའི་ཞུ་དག་ཕྱིར་འཐེན་པ།</translation>
    </message>
</context>
<context>
    <name>UIcolorEditor</name>
    <message>
        <source>Contrast Color</source>
        <translation>ཁ་དོག་སྙོམས་སྒྲིག</translation>
    </message>
    <message>
        <source>UI Color Editor</source>
        <translation>སྐུ་མགྲོན་ཡོད་སའི་ངོས་ཀྱི་ཁ་དོག་སྙོམས་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Main Color</source>
        <translation>ཁ་དོག་གཙོ་བོ།</translation>
    </message>
    <message>
        <source>Click on this button to set the edition mode of the contrast color.</source>
        <translation>མཐེབ་སྣོན་བཀོལ་ནས་ཁ་དོག་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Change the color moving the selectors and see the changes in the preview buttons.</source>
        <translation>ཁ་དོག་བརྗེ་འགྱུར་ལ་སྔོན་བལྟའི་མཐེབ་སྣོན་གྱིས་ལྟ་ཐུབ།</translation>
    </message>
    <message>
        <source>Click on this button to set the edition mode of the main color.</source>
        <translation>མཐེབ་སྣོན་བཀོལ་ནས་མདོག་གཙོ་བོ་བཀོད་སྒྲིག་་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Select a Button to modify it</source>
        <translation>མཐེབ་སྣོན་གཅིག་འདེམ་ནས་དག་བཅོས་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <source>&amp;Quit</source>
        <translation>ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>&amp;Configuration</source>
        <translation> ཡིག་ཆ་སྡེབ་སྒྲིག</translation>
    </message>
    <message>
        <source>&amp;Open Screenshot Option</source>
        <translation>འདྲ་སྦྱར་འདེམ་གསེས་ཁ་ཕཡེ་བ།(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Take Screenshot</source>
        <translation>བརྙན་ཡོལ་བཅད་རིས་(&amp;S)</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation> ཡོལ་གཏུབ།</translation>
    </message>
    <message>
        <source>Unable to use kylin-screenshot</source>
        <translation>ཆིན་ལིན་འདྲ་སྦྱར་པར་བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <source>&amp;ShortCut</source>
        <translation>མྱུར་མཐེབ།</translation>
    </message>
</context>
<context>
    <name>CaptureLauncher</name>
    <message>
        <source>Area:</source>
        <translation>ཁུལ་ཁོངས།:</translation>
    </message>
    <message>
        <source>Take shot</source>
        <translation>འདྲ་སྦྱར་མགོ་བརྩམས་པ།</translation>
    </message>
    <message>
        <source>Full Screen (All Monitors)</source>
        <translation>ངོས་ཡོངས་འདྲ་སྦྱར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Delay:</source>
        <translation>ཕྱིར་འགྱངས།</translation>
    </message>
    <message>
        <source>Rectangular Region</source>
        <translation>རིས་དབྱིབས་འདེམ་ཁོངས།</translation>
    </message>
    <message>
        <source> second</source>
        <translation>0 སྐར་ཆ།</translation>
    </message>
    <message>
        <source> seconds</source>
        <translation>0 སྐར་ཆ།</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation> ཡོལ་གཏུབ།</translation>
    </message>
    <message>
        <source>Capture Mode</source>
        <translation>རྣམ་པ་བཙལ་བ།</translation>
    </message>
</context>
<context>
    <name>ArrowTool</name>
    <message>
        <source>Arrow</source>
        <translation>མདའ་རྩེ།</translation>
    </message>
    <message>
        <source>Set the Arrow as the paint tool</source>
        <translation>མདའ་མགོ་འདེམ་ནས་པར་རིས་ཡོ་བྱད་འཚོལ།</translation>
    </message>
</context>
<context>
    <name>AppLauncherWidget</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Launch in terminal</source>
        <translation>མཐས་སྣེ་ནས་སྒོ་ཕྱེ།</translation>
    </message>
    <message>
        <source>Unable to write in</source>
        <translation>%sའབྲི་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Open With</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Unable to launch in terminal.</source>
        <translation>མཐའ་སྣེ་ནས་སྒོ་འབྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Keep open after selection</source>
        <translation>འདེམ་རྗེས་སྒེའུ་ཁུང་སྒོ་ཕྱེ་དགོས།</translation>
    </message>
</context>
<context>
    <name>GeneneralConf</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>བསྐྱར་འཇོག</translation>
    </message>
    <message>
        <source>Configuration File</source>
        <translation> ཡིག་ཆ་སྡེབ་སྒྲིག</translation>
    </message>
    <message>
        <source>Unable to write file.</source>
        <translation>%sའབྲི་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Launch at startup</source>
        <translation>སྒོ་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Show the help message at the beginning in the capture mode.</source>
        <translation>མ་བླངས་རྗེས་རོགས་རམ་བརྡ་འབྱོར་ཐུབ།</translation>
    </message>
    <message>
        <source>Close after capture</source>
        <translation>བླངས་རྗེས་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>ཕྱིར་འདྲེན།</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>འདྲེན་འཇུག</translation>
    </message>
    <message>
        <source>Confirm Reset</source>
        <translation>བསྐྱར་བཟོ་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Show help message</source>
        <translation>རོགས་རམ་གྱི་བརྡ་འཕྲིན་འཆར་བ། </translation>
    </message>
    <message>
        <source>Copy URL after upload</source>
        <translation>ཡར་བསྐུར་རྗེས་URLརུ་འདྲ་བཟོ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Show tray icon</source>
        <translation>འདེགས་སྡེར་པར་རིས་མངོན་པ།</translation>
    </message>
    <message>
        <source>Unable to read file.</source>
        <translation>%s ཀློག་ཐབས་མེད།</translation>
    </message>
    <message>
        <source>Close after taking a screenshot</source>
        <translation>རྩིས་འཁོར་ངོས་འདྲ་སྦྱར་པར་བླངས་རྗེས་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Save File</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>Show the systemtray icon</source>
        <translation>ལས་འགན་མཚོན་རྟགས་མངོན་པ།</translation>
    </message>
    <message>
        <source>Show desktop notifications</source>
        <translation>ཅོག་ངོས་སྔོན་བཀོད་དྲན་སྐུལ།</translation>
    </message>
    <message>
        <source>Are you sure you want to reset the configuration?</source>
        <translation>སྒྲིག་ཆས་རང་བསྐྱར་འདེམ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>file name can not contains '/'</source>
        <translation>ཡིག་ཚགས་ཉར་ཚགས་བྱེད་དུས་མིང་གི་ཁྲོད་ཡིག་'/'ཡོད་མི་ཆོག</translation>
    </message>
    <message>
        <source>Capture saved as </source>
        <translation>ཉར་ཚགས་ཐུབ་པ་མངོན་པ།</translation>
    </message>
    <message>
        <source>Error trying to save as </source>
        <translation>གནས་གཞན་དུ་ཉར་བ་དུས་ཚོད་ནོར་བ།</translation>
    </message>
    <message>
        <source>Unable to connect via DBus</source>
        <translation>Dbusབརྒྱུད་ནས་འབྲེལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Save Error</source>
        <translation>ཉར་ཚགས་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Capture saved to clipboard</source>
        <translation>འདྲ་སྦྱར་ངོས་སུ་ཉར་ཚགས་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>can not save  because filename too long</source>
        <translation>ཡིག་ཆའི་མིང་རིང་དྲགས་པས་ཉར་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>can not save file as hide file</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱེད་དུས་ལྐོག་ཉར་བྱེད་མི་ཆོག</translation>
    </message>
    <message>
        <source>Kylin-Screenshot</source>
        <translation>ཆིན་ལིན་འདྲ་སྦྱར་པར།</translation>
    </message>
    <message>
        <source>Unable to write in</source>
        <translation>%sའབྲི་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>kylin-screenshot</source>
        <translation>ཆིན་ལིན་འདྲ་སྦྱར་པར།</translation>
    </message>
    <message>
        <source>URL copied to clipboard.</source>
        <translation>སྦྲེལ་ཐག་འདྲ་བཟོ་བྱས་ནས་འདྲ་སྦྱར་ངོས་སུ་འཇོག་པ།</translation>
    </message>
</context>
<context>
    <name>StrftimeChooserWidget</name>
    <message>
        <source>Second (00-59)</source>
        <translation>སྐར་ཆ（00-59）</translation>
    </message>
    <message>
        <source>Week Day (1-7)</source>
        <translation>གཟའ་འཁོར་གཅིག་གི་ཉི་མ（1-7）</translation>
    </message>
    <message>
        <source>Year (2000)</source>
        <translation>ལོ（2000）</translation>
    </message>
    <message>
        <source>Month Name (january)</source>
        <translation>ཟླ་བ（ཟླ་དང་པོ - ཟླ་བཅུ་གཉིས་པ）</translation>
    </message>
    <message>
        <source>Full Date (%m/%d/%y)</source>
        <translation>དུས་ཚོད་ཆ་ཚང་།（%m/%d/%y）</translation>
    </message>
    <message>
        <source>Day Name (monday)</source>
        <translation>གཟའ་འཁོར（གཟའ་ཟླ་བ - གཟའ་ཉི་མ）</translation>
    </message>
    <message>
        <source>Day (001-366)</source>
        <translation>ཉིན（001-366）</translation>
    </message>
    <message>
        <source>Full Date (%Y-%m-%d)</source>
        <translation>དུས་ཚོད་ཆ་ཚང་།（%Y-%m-%d）</translation>
    </message>
    <message>
        <source>Minute (00-59)</source>
        <translation>སྐར་མ（00-59）</translation>
    </message>
    <message>
        <source>Day (01-31)</source>
        <translation>ཉི་མ（01-31）</translation>
    </message>
    <message>
        <source>Time (%H-%M-%S)</source>
        <translation>དུས་ཚོད（%H-%M-%S）</translation>
    </message>
    <message>
        <source>Day of Month (1-31)</source>
        <translation>ཟླ་གཅིག་གི་ཉི་མ་ག་གེ་མོ（1-31）</translation>
    </message>
    <message>
        <source>Month Name (jan)</source>
        <translation>ཟླ་བ（ཟླ་་དང་པོ - ཟླ་བཅུ་གཉིས）</translation>
    </message>
    <message>
        <source>Century (00-99)</source>
        <translation>དུས་རབས（00-99）</translation>
    </message>
    <message>
        <source>Year (00-99)</source>
        <translation>ལོ（00-99）</translation>
    </message>
    <message>
        <source>Week (01-53)</source>
        <translation>གཟའ་འཁོར（01-53）</translation>
    </message>
    <message>
        <source>Time (%H-%M)</source>
        <translation>དུས་ཚོད（%H-%M）</translation>
    </message>
    <message>
        <source>Month (01-12)</source>
        <translation>ཟླ་བ(01-12)</translation>
    </message>
    <message>
        <source>Day Name (mon)</source>
        <translation>གཟའ་འཁོར（དང་པོ - བདུན་པ）</translation>
    </message>
    <message>
        <source>Hour (00-23)</source>
        <translation>ཆུ་ཚོད（00-23）</translation>
    </message>
    <message>
        <source>Hour (01-12)</source>
        <translation>དུས་ཚོད（01-12）</translation>
    </message>
</context>
<context>
    <name>SidePanelWidget</name>
    <message>
        <source>Active color:</source>
        <translation>བྱ་བྱེད་ཁ་དོག</translation>
    </message>
    <message>
        <source>Grab Color</source>
        <translation>ཁ་དོག་ལརན་པ།</translation>
    </message>
    <message>
        <source>Active thickness:</source>
        <translation>མིག་སྔའི་ཞེང་ཚད།</translation>
    </message>
    <message>
        <source>Press ESC to cancel</source>
        <translation> ESCསྣོན་ཆས་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>SizeIndicatorTool</name>
    <message>
        <source>Show the dimensions of the selection (X Y)</source>
        <translation>ཆེ་ཆུང་འདེམ་པ་མངོན་པ།(X Y)</translation>
    </message>
    <message>
        <source>Selection Size Indicator</source>
        <translation>ཆེ་ཆུང་གི་རྟགས་འདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>ImgurUploader</name>
    <message>
        <source>Upload to Imgur</source>
        <translation>Imgurརུ་བསྐུར་བ།</translation>
    </message>
    <message>
        <source>Delete image</source>
        <translation>བརྙན་རིས་སུབ་པ། (_D)</translation>
    </message>
    <message>
        <source>Copy URL</source>
        <translation>འདྲ་བཟོ་སྦྲེལ་ཐག</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation>URLཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Uploading Image</source>
        <translation>ཡར་བསྐུར་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Unable to open the URL.</source>
        <translation>སྒྲེལ་ཐག་ཁ་འབྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Screenshot copied to clipboard.</source>
        <translation>འདྲ་བཟོ་དྲས་སྦྱར་ངོས་སུ་འཇོག་པ།</translation>
    </message>
    <message>
        <source>URL copied to clipboard.</source>
        <translation>སྦྲེལ་ཐག་འདྲ་བཟོ་བྱས་ནས་འདྲ་སྦྱར་ངོས་སུ་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Image to Clipboard.</source>
        <translation>དྲས་སྦྱར་ངོས་སུ་ཡིག་ཆ་ཉར་ཚགས་བྱེད།</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <source>options tool</source>
        <translation>པར་ཉར་ཚགས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>འདེམས་ཚན།</translation>
    </message>
</context>
<context>
    <name>ShortCutWidget</name>
    <message>
        <source>Save selection as a file</source>
        <translation>ཉར་ཚགས་ཀྱི་ཡིག་ཆ་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Move selection 1px</source>
        <translation>འདེམས་ཁུལ་སྤོ་བ།</translation>
    </message>
    <message>
        <source>Capture Screen selection</source>
        <translation>ཡུལ་ཁོངས་ཀྱི་པར་ལེན་པ།</translation>
    </message>
    <message>
        <source>Mouse Wheel</source>
        <translation>ཙིག་མདའི་འདྲིལ་འཁོར།</translation>
    </message>
    <message>
        <source>Change the tool's thickness</source>
        <translation>ཡོ་བྱད་ཀྱི་མཐུག་ཚད་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation>དྲས་སྦྱར་པང་ནང་འདྲ་ཕབ་བྱེད་པ། (&amp;C)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>ཞིབ་བརྗོད།</translation>
    </message>
    <message>
        <source>Capture Top Screen</source>
        <translation>སྒེའུ་ཁུང་གི་ཁར་ལེན་པ།</translation>
    </message>
    <message>
        <source>Undo the last modification</source>
        <translation>ཐེངས་སྔོན་མའི་ཞུ་དག་ཕྱིར་འཐེན་པ།</translation>
    </message>
    <message>
        <source>Resize selection 1px</source>
        <translation>ཆེ་ཆུང་སྙོམས་སྒྲིག་འདེམ་པ། 1 px</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation> ཡོལ་གཏུབ།</translation>
    </message>
    <message>
        <source>Quit capture</source>
        <translation>ལེན་པ་ནས་ཕྱིར་ནུར་བ།</translation>
    </message>
    <message>
        <source>Capturn Full Screen</source>
        <translation>ངོས་ཡོངས་པར་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Keypress</source>
        <translation>མཐེབ་སྣོན།</translation>
    </message>
    <message>
        <source>ShortCut</source>
        <translation>མྱུར་མཐེབ།</translation>
    </message>
</context>
<context>
    <name>SaveAsTool</name>
    <message>
        <source>Save the capture.....</source>
        <translation>གནས་གཞན་དུ་ཉར་བ།....</translation>
    </message>
    <message>
        <source>saveas</source>
        <translation>གནས་གཞན་དུ་ཉར་བ།</translation>
    </message>
</context>
<context>
    <name>infoWidget</name>
    <message>
        <source>Screenshot is an easy to use application.that supports the basic screenshot function,but also provides the draw rectangle tool, draw a circle tool, blur, add annotations, add text and other functions</source>
        <translation>ཡི་གེ་སྣོན་པ།</translation>
    </message>
    <message>
        <source>SUPPORT:%1</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ།%1</translation>
    </message>
    <message>
        <source>screenshot</source>
        <translation>བརྙན་ཡོལ་བཅད་རིས་(&amp;S)</translation>
    </message>
    <message>
        <source>version:v1.0.0</source>
        <translation>པར་གཞི།v1.0.0</translation>
    </message>
</context>
<context>
    <name>MySaveDialog</name>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ</translation>
    </message>
</context>
<context>
    <name>CircleTool</name>
    <message>
        <source>Circle</source>
        <translation>སྒོར་ཐིག</translation>
    </message>
    <message>
        <source>Set the Circle as the paint tool</source>
        <translation>ཨ་ལོང་དབྱིབས་རི་མོའི་ཡོ་བྱད་དུ་འདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>VisualsEditor</name>
    <message>
        <source>Opacity of area outside selection:</source>
        <translation>ཡུལ་ཁོངས་ཕྱི་རོལ་གྱི་མི་གསལ་བའི་ཚད་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Button Selection</source>
        <translation>མཐེབ་གཞོང་འདེམ་གསེས།</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>ཡོད་ཚད་འདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>DBusUtils</name>
    <message>
        <source>Unable to connect via DBus</source>
        <translation>Dbusབརྒྱུད་ནས་འབྲེལ་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>AppLauncher</name>
    <message>
        <source>Choose an app to open the capture</source>
        <translation>བཀོལ་སྤྱོད་གཅིག་འདེམ་ནས་ཁ་ཕྱེ་རྗེས་བཅད་ཡོལ་བྱ་ཐུབ།</translation>
    </message>
    <message>
        <source>App Launcher</source>
        <translation>སྒོ་འབྱེད་ཆས་བཀོལ་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>MarkerTool</name>
    <message>
        <source>Marker</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Set the Marker as the paint tool</source>
        <translation>མཚོན་རྟགས་པར་རིས་ཀྱི་ཡོ་བྱད་དུ་བཀོལ།</translation>
    </message>
</context>
<context>
    <name>PencilTool</name>
    <message>
        <source>Pencil</source>
        <translation>ཞ་སྨྱུག</translation>
    </message>
    <message>
        <source>Set the Pencil as the paint tool</source>
        <translation>ཞ་སྨྱུག་པར་རིས་འབྲི་བྱེད་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>ImgurUploaderTool</name>
    <message>
        <source>Image Uploader</source>
        <translation>པར་རིས་ཡར་བསྐུར་བ།</translation>
    </message>
    <message>
        <source>Upload the selection to Imgur</source>
        <translation>ཡར་བསྐུར་ས་ Imgurའདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>ConfigWindow</name>
    <message>
        <source>Filename Editor</source>
        <translation>ཡིག་ཆའི་མིང་འབྲི་བྱེད།</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>སྡེབ་སྒྲིག</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>མཚམས་ངོས།</translation>
    </message>
    <message>
        <source>General</source>
        <translation>རྒྱུན་སྲོལ།</translation>
    </message>
</context>
<context>
    <name>LuPing</name>
    <message>
        <source>luping</source>
        <translation>བཅད་ཡོལ་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>CaptureWidget</name>
    <message>
        <source>save as</source>
        <translation>གནས་གཞན་པར་ཉར་ཚགས་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Unable to capture screen</source>
        <translation>གློག་ཀླད་ངོས་པར་རྒྱག་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>RectangleTool</name>
    <message>
        <source>Rectangle</source>
        <translation>ཟུར་གྲུ་བཞི་དབྱིབས།</translation>
    </message>
    <message>
        <source>Set the Rectangle as the paint tool</source>
        <translation>རི་མོའི་ཡོ་བྱད་བཀོད་སྒྲིག</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <source>Screenshot</source>
        <translation> ཡོལ་གཏུབ།</translation>
    </message>
    <message>
        <source>ScreenShot Info</source>
        <translation>པར་བསྐྱར་ལེན། བརྡ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>Save_Location</name>
    <message>
        <source>save location</source>
        <translation>གནས་ས་ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>save type</source>
        <translation>ཉར་ཚགས་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>Save_Location2</name>
    <message>
        <source>save location</source>
        <translation>གནས་ས་ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>save type</source>
        <translation>ཉར་ཚགས་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>SelectionTool</name>
    <message>
        <source>Set the rectangle as the paint tool</source>
        <translation>རི་མོ་འབྲི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>ScreenGrabber</name>
    <message>
        <source>Unable to capture screen</source>
        <translation>གློག་ཀླད་ངོས་པར་རྒྱག་མི་ཐུབ།</translation>
    </message>
</context>
</TS>