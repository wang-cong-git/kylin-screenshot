/*
 *
 * Copyright: 2020 KylinSoft Co., Ltd.
 * Authors:
 *   huanhuan zhang <zhanghuanhuan@kylinos.cn>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "shortcutwidget.h"
#include <QTableWidget>
#include <QHeaderView>
#include <QPainter>
#include <QPushButton>
ShortCutWidget::ShortCutWidget(QWidget *parent) :
    QWidget(parent)
{
    setWindowIcon(QIcon::fromTheme("kylin-screenshot"));

    MotifWmHints hints;
    hints.flags = MWM_HINTS_FUNCTIONS | MWM_HINTS_DECORATIONS;
    hints.functions = MWM_FUNC_ALL;
    hints.decorations = MWM_DECOR_BORDER;
    XAtomHelper::getInstance()->setWindowMotifHint(winId(), hints);

    m_titleIcon_label = new  QLabel(this);
    m_titleIcon_label->setPixmap(QPixmap(QIcon::fromTheme("kylin-screenshot").pixmap(QSize(24,
                                                                                           24))));
    m_titleIcon_label->move(8, 8);
    m_titleName_label = new QLabel(this);
    font.setFamily("Noto Sans CJK SC Medium");
    font.setPixelSize(14);
    m_titleName_label->setFont(font);
    m_titleName_label->setText(tr("Screenshot"));
    m_titleName_label->setAlignment(Qt::AlignLeft);
    m_titleName_label->setFixedSize(170, 24);
    m_titleName_label->move(40, 8);
    m_exit_btn = new QPushButton(this);
    m_exit_btn->setIcon(QIcon::fromTheme("window-close-symbolic"));
    m_exit_btn->setFixedSize(24, 24);
    m_exit_btn->setIconSize(QSize(16, 16));
    m_exit_btn->setProperty("isWindowButton", 0x2);
    m_exit_btn->setProperty("useIconHighlightEffect", 0x8);
    m_exit_btn->setFlat(true);
    m_exit_btn->move(368, 8);
    connect(m_exit_btn, &QPushButton::clicked,
            this, &ShortCutWidget::close);
    m_min_btn = new QPushButton(this);
    m_min_btn->setIcon(QIcon::fromTheme("window-minimize-symbolic"));
    m_min_btn->setFixedSize(24, 24);
    m_min_btn->setIconSize(QSize(16, 16));
    m_min_btn->move(336, 8);
    m_min_btn->setProperty("isWindowButton", 0x1);
    m_min_btn->setProperty("useIconHighlightEffect", 0x2);
    m_min_btn->setFlat(true);
    connect(m_min_btn, &QPushButton::clicked,
            this, &ShortCutWidget::hide);
    tableName = new  QLabel(this);
    font.setFamily("Noto Sans CJK SC Medium");
    font.setPixelSize(16);
    tableName->setFont(font);
    tableName->setText(tr("ShortCut"));
    tableName->move(25, 56);
    setFixedSize(400, 546);
    initLabel();
}

void ShortCutWidget:: initLabel()
{
    for (int i = 0; i < m_keys.size(); i++) {
        QLabel *lb = new  QLabel(this);
        lb->setFixedSize(352, 38);
        QLabel *labelchild1 = new QLabel;
        QLabel *labelchild2 = new QLabel;
        labelchild1->setText(tr(m_description.at(i)));
        font.setPixelSize(14);
        labelchild1->setFont(font);
        labelchild2->setFont(font);
        labelchild1->setAlignment(Qt::AlignLeft);
        labelchild2->setText(tr(m_keys.at(i)));
        QHBoxLayout *m_layout = new  QHBoxLayout;
        labelchild1->setFixedSize(208, 38);
        m_layout->addWidget(labelchild1);
        m_layout->addWidget(labelchild2);
        lb->setLayout(m_layout);
        labels.append(lb);
    }
    for (int i = 0; i < m_keys.size(); i++) {
        labels.at(i)->move(25, 96+i*38);
    }
}

QVector<const char *> ShortCutWidget::m_keys =
{
    QT_TR_NOOP("Keypress"),
    "PrtSc",
    "Ctrl + PrtSc",
    "Shift + PrtSc",
    "←↓↑→",
    "SHIFT + ←↓↑→",
    "ESC",
    "CTRL + C",
    "CTRL + S",
    "CTRL + Z",
    QT_TR_NOOP("Mouse Wheel")
};

QVector <const char *> ShortCutWidget::m_description =
{
    QT_TR_NOOP("Description"),
    QT_TR_NOOP("Capturn Full Screen"),
    QT_TR_NOOP("Capture Top Screen"),
    QT_TR_NOOP("Capture Screen selection"),
    QT_TR_NOOP("Move selection 1px"),
    QT_TR_NOOP("Resize selection 1px"),
    QT_TR_NOOP("Quit capture"),
    QT_TR_NOOP("Copy to clipboard"),
    QT_TR_NOOP("Save selection as a file"),
    QT_TR_NOOP("Undo the last modification"),
    QT_TR_NOOP("Change the tool's thickness")
};

void ShortCutWidget::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    painter.setBrush(QBrush(QColor(95, 95, 95)));
    painter.setOpacity(0.10);
    painter.setPen(QColor(95, 95, 95));
    for (int i = 0; i < m_keys.size(); i++) {
        if (0 != i%2) {
            painter.drawRoundedRect(QRect(labels.at(i)->geometry()), 6, 6, Qt::AbsoluteSize);
        }
    }
}

ShortCutWidget::~ShortCutWidget()
{
}
